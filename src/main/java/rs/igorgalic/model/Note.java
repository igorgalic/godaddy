/**
 * 
 */
package rs.igorgalic.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

/**
 * @author igor
 *
 */
@Entity
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, property = "dtype", visible = true)
@JsonSubTypes({
	@JsonSubTypes.Type(value = TextNote.class, name = "TextNote"),
	@JsonSubTypes.Type(value = ImageNote.class, name = "ImageNote"),
	@JsonSubTypes.Type(value = LinkNote.class, name = "LinkNote")})
public abstract class Note {
	
	@Id
	@GeneratedValue
	public long noteId;
	
	public boolean archived;
	
	@UpdateTimestamp
	public LocalDateTime lastModified;
	
	public Note() { }

	public Note(boolean archived, LocalDateTime lastModified) {
		super();
		this.archived = archived;
		this.lastModified = lastModified;
	}

	/**
	 * @return the dtype
	 */
	public String getDtype() {
		return this.getClass().getSimpleName();
	}
	
	/**
	 * @return the noteId
	 */
	public long getNoteId() {
		return noteId;
	}

	/**
	 * @param noteId the noteId to set
	 */
	public void setNoteId(long noteId) {
		this.noteId = noteId;
	}

	/**
	 * @return the archived
	 */
	public boolean isArchived() {
		return archived;
	}

	/**
	 * @param archived the archived to set
	 */
	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	/**
	 * @return the lastModified
	 */
	public LocalDateTime getLastModified() {
		return lastModified;
	}

	/**
	 * @param lastModified the lastModified to set
	 */
	public void setLastModified(LocalDateTime lastModified) {
		this.lastModified = lastModified;
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Note [noteId=" + noteId + ", archived=" + archived + ", lastModified=" + lastModified + ", getNoteId()="
				+ getNoteId() + ", isArchived()=" + isArchived() + ", getLastModified()=" + getLastModified() + "]";
	};
	
	

}
