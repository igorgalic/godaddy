/**
 * 
 */
package rs.igorgalic.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * @author igor
 *
 */
@Entity
public class LinkNote extends Note {
	
	// allow long links
	@Column(columnDefinition="TEXT")
	public String linkURL;

	/**
	 * 
	 */
	public LinkNote() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param archived
	 * @param lastModified
	 */
	public LinkNote(boolean archived, LocalDateTime lastModified) {
		super(archived, lastModified);
		// TODO Auto-generated constructor stub
	}

	/** 
	 * Virtual field
	 * Workaround for the problem of Jackson do not serialise collections
	 *  for the inherited types 
	 */
	@Override
	public String getDtype() {
		return LinkNote.class.getSimpleName();
	}
	
	
	/**
	 * @return the linkURL
	 */
	public String getLinkURL() {
		return linkURL;
	}

	/**
	 * @param linkURL the linkURL to set
	 */
	public void setLinkURL(String linkURL) {
		this.linkURL = linkURL;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LinkNote [linkURL=" + linkURL + ", noteId=" + noteId + ", archived="
				+ archived + ", lastModified=" + lastModified + "]";
	}
	
	

}
