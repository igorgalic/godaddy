/**
 * 
 */
package rs.igorgalic.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author igor
 *
 */
@Entity
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, property = "dtype", visible = true)
public class TextNote extends Note {
	
	// allow long titles
	@Column(columnDefinition="TEXT")
	public String title;
	
	// allow long content
	@Column(columnDefinition="TEXT")
	public String content;
	public String color;

	/**
	 * 
	 */
	public TextNote() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param archived
	 * @param lastModified
	 */
	public TextNote(boolean archived, LocalDateTime lastModified) {
		super(archived, lastModified);
		// TODO Auto-generated constructor stub
	}

	/** 
	 * Virtual field
	 * Workaround for the problem of Jackson do not serialise collections
	 *  for the inherited types 
	 */
	@Override
	public String getDtype() {
		return TextNote.class.getSimpleName();
	}
	
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TextNote [title=" + title + ", content=" + content + ", color=" + color + ", noteId=" + noteId
				+ ", archived=" + archived + ", lastModified=" + lastModified + "]";
	}
		
}
