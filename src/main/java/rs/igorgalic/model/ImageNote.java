/**
 * 
 */
package rs.igorgalic.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * @author igor
 *
 */
@Entity
public class ImageNote extends Note {
	
	// allow long image URLs
	@Column(columnDefinition="TEXT")
	public String imageSrc;

	/**
	 * 
	 */
	public ImageNote() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param archived
	 * @param lastModified
	 */
	public ImageNote(boolean archived, LocalDateTime lastModified) {
		super(archived, lastModified);
		// TODO Auto-generated constructor stub
	}
	
	/** 
	 * Virtual field
	 * Workaround for the problem of Jackson do not serialise collections
	 *  for the inherited types 
	 */
	@Override
	public String getDtype() {
		return ImageNote.class.getSimpleName();
	}

	/**
	 * @return the imageSrc
	 */
	public String getImageSrc() {
		return imageSrc;
	}

	/**
	 * @param imageSrc the imageSrc to set
	 */
	public void setImageSrc(String imageSrc) {
		this.imageSrc = imageSrc;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ImageNote [imageSrc=" + imageSrc + ", noteId=" + noteId + ", archived=" + archived + ", lastModified="
				+ lastModified + "]";
	}
	
		

}
