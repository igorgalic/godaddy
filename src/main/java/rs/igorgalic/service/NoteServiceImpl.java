/**
 * 
 */
package rs.igorgalic.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.igorgalic.model.Note;

/**
 * @author igor
 *
 */
@Service("NoteService")
@Transactional
public class NoteServiceImpl implements NoteService {
	
	@Autowired
	private NoteRepository noteDAO;

	/* (non-Javadoc)
	 * @see rs.igorgalic.service.NoteService#findAll()
	 */
	@Override
	public Iterable<Note> findAll() {
		return noteDAO.findAll();
	}
	
	/* (non-Javadoc)
	 * @see rs.igorgalic.service.NoteService#findAll(java.lang.String dtype)
	 */
	@Override
	public Iterable<Note> findAll(String dtype) {
		return noteDAO.fetchNotesByType(dtype);
	}

	/* (non-Javadoc)
	 * @see rs.igorgalic.service.NoteService#findAll(java.lang.Boolean isArchived)
	 */
	@Override
	public Iterable<Note> findAll(Boolean isArchived) {
		return noteDAO.fetchNotesByArchived(isArchived);
	}

	
	/* (non-Javadoc)
	 * @see rs.igorgalic.service.NoteService#find(long)
	 */
	@Override
	public Optional<Note> find(long id) {
		return noteDAO.findById(id);
	}

	/* (non-Javadoc)
	 * @see rs.igorgalic.service.NoteService#save(rs.igorgalic.model.Note)
	 */
	@Override
	public Note save(Note note) {
		return noteDAO.save(note);

	}

	/* (non-Javadoc)
	 * @see rs.igorgalic.service.NoteService#remove(long)
	 */
	@Override
	public void remove(long id) {
		noteDAO.deleteById(id);

	}

	@Override
	public void clearTrash() {
		noteDAO.clearTrash();
		
	}

}
