/**
 * 
 */
package rs.igorgalic.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import rs.igorgalic.model.Note;

/**
 * @author igor
 *
 */
@Repository
public interface NoteRepository extends CrudRepository<Note, Long> {
	
	@Query("SELECT n FROM Note as n WHERE dtype=:noteType and archived=0 ORDER BY last_modified DESC")
    List<Note> fetchNotesByType(@Param("noteType") String dtype);
	
	@Query("SELECT n FROM Note as n WHERE archived=:isArchived ORDER BY last_modified DESC")
    List<Note> fetchNotesByArchived(@Param("isArchived") Boolean isArchived);
	
	@Transactional
	@Modifying
	@Query("DELETE FROM Note as n WHERE archived!=0")
	void clearTrash();

}
