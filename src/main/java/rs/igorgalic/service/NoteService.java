/**
 * 
 */
package rs.igorgalic.service;

import java.util.Optional;

import rs.igorgalic.model.Note;

/**
 * @author igor
 *
 */
public interface NoteService {
	
	public Iterable<Note> findAll();
	
	public Iterable<Note> findAll(String dtype);
	
	public Iterable<Note> findAll(Boolean isArchived);
	
	public Optional<Note> find(long id);
	
	public Note save(Note note);
	
	public void remove(long id);
	
	public void clearTrash();

}
