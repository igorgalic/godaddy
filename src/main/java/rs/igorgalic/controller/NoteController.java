/**
 * 
 */
package rs.igorgalic.controller;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import rs.igorgalic.model.ImageNote;
import rs.igorgalic.model.LinkNote;
import rs.igorgalic.model.Note;
import rs.igorgalic.model.TextNote;
import rs.igorgalic.service.NoteService;

/**
 * @author igor
 *
 */
@RestController
@RequestMapping("notes")
public class NoteController {
	
	@Autowired
	private NoteService noteService;
	
	 @CrossOrigin
	 @PostMapping(path="getall")
	 public @ResponseBody Iterable<Note> listNotes(@RequestParam @Null String type)
		{
		 if (type==null) return noteService.findAll(new Boolean(false));
		 
		 if (type.equalsIgnoreCase("archive")) 
			 return noteService.findAll(new Boolean(true));
		 if (type.equalsIgnoreCase(TextNote.class.getSimpleName())) 
			 return noteService.findAll(TextNote.class.getSimpleName());
		 if (type.equalsIgnoreCase(ImageNote.class.getSimpleName())) 
			 return noteService.findAll(ImageNote.class.getSimpleName());
		 if (type.equalsIgnoreCase(LinkNote.class.getSimpleName())) 
			 return noteService.findAll(LinkNote.class.getSimpleName());
		 
		 // Default handler is 'all'
		 return noteService.findAll(new Boolean(false));
		}
	 	 
	 @CrossOrigin
	 @PostMapping(path="saveNote")
	 public @ResponseBody Note saveNote(@RequestBody @NotNull Note note)
		{ 
		 return noteService.save(note);
		}
	 
	 
	 
	 @CrossOrigin
	 @PostMapping(path="removeNote")
	 public void removeNote(@RequestParam @NotNull Long noteId)
		{ 
		 noteService.remove(noteId);
		}
	 
	 @CrossOrigin
	 @PostMapping(path="clearTrash")
	 public void clearTrash()
		{ 
		 noteService.clearTrash();
		}

}
