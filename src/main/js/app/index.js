import React from "react";
import {render} from "react-dom";
import {BrowserRouter} from "react-router-dom";
import {Switch,Route,Redirect} from "react-router-dom";
import { Menu } from "./components/Menu";
import { AllNotes } from "./components/AllNotes";
import { Trash } from "./components/Trash";
import { TextNotesList } from "./components/TextNotesList";
import { ImageNotesList } from "./components/ImageNotesList";
import { LinkNotesList } from "./components/LinkNotesList";
import { ConfirmDialog } from "./components/ConfirmDialog";

class App extends React.Component {
    render() {
        return (
            <BrowserRouter>    
                <div className="wrapper">        
                    <div id="sidebar">
                        <Menu/>
                    </div>            
                    <div id="content">
                    <Switch>
                        <Route exact path="/" render={props => <Redirect to="/allnotes" />}/>
                        <Route path="/allnotes" render={props => <AllNotes />}/>
                        <Route path="/textnotes" render={props => <TextNotesList />}/>
                        <Route path="/imagenotes" render={props => <ImageNotesList />}/>
                        <Route path="/linknotes" render={props => <LinkNotesList />}/>
                        <Route path="/trash" render={props => <Trash />}/>
                    </Switch>
                    </div>
                    <ConfirmDialog />         
                </div>
            </BrowserRouter>
        );
    }
}

render(<App />, window.document.getElementById('app'));