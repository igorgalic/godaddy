import * as constants from "../constants"
import React from "react";
import {AddEditNoteDialog} from "./AddEditNoteDialog";
import { Redirect } from "react-router-dom"

export class Header extends React.Component {
    constructor(props) {
        super(props);
        this.props = props;

        this.state = {
            result: false
        }
      }

      onClearTrash(){
        this.setState({
            result: false
        });
        $.ajax({
            url: constants.REST_URL+'/clearTrash',
            type: 'post',
            dataType: 'json',
            headers: {"Access-Control-Allow-Origin": "*"},
            contentType: 'application/json',
            success: function (data) {
                this.setState({
                    result: true
                });
                
            }.bind(this)
        });
        this.props.callback();
      }

    render() {
        if (this.state.result===true) return <Redirect to="/trash"/>;
        if (this.props.isArchive) // Archive header render
        return (
            <div id="contentTitle" className="container"> 
              <span className="float-left"> Trash </span>
                <button onClick={this.onClearTrash.bind(this)} type="button" className="btn btn-defaut btn-red btn-lg round float-right">Clear trash</button>
            </div>
        );
        else // Regular header render
        return (
            <div id="contentTitle" className="container"> 
              <span className="float-left"> Notes </span>
                <button type="button" className="btn btn-defaut btn-green btn-lg round float-right"  data-toggle="modal" data-target="#addeditnotemodal0">Add New Note</button>
                <AddEditNoteDialog key="headerAddEdit" callback={this.props.callbackEdit}/>
            </div>
 
        );
    }
}