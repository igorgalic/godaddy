import React from "react";
import { Redirect } from "react-router-dom";


export class EmptyTrash extends React.Component {

    constructor(){
        super();
        this.state = {
            redirect: false
          }
    }

    handleClick(){
        this.setState({redirect: true});
    }
    
    render() {
        if (this.state.redirect) return <Redirect to="/allnotes"/>;
        return (
            <div id="emptyTrash">
                <div id="emptyTrashImg">       
                <img src="./Illustrations/notes-empty-state.svg" alt="EmptyTrash Image"/>
                </div>
                <div id="emptyTrashText">       
                   You have no more files in the trash
                </div>
                <div id="emptyTrashButton">       
                   <button onClick={this.handleClick.bind(this)} className="btn btn-defaut btn-green btn-lg round">All notes</button>
                </div>
            </div>
        );
    }
}