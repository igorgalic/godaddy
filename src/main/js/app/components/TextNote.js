import * as constants  from "../constants";
import PropTypes from "prop-types";
import React from "react";
import {UncontrolledTooltip,Popover,PopoverBody } from "reactstrap";

export class TextNote extends React.Component {
    constructor(props){
        super(props);
        this.state= {
            noteId: props.noteId,
            dtype: props.dtype,
            title: props.title,
            text: props.text,
            style: props.className,
            archived: props.archived,
            hover: false,
            popoverOpen: false,
            callback: props.callback
        }
    }

    hoverOn(){
        this.setState({ hover: true });
      }
    hoverOff(){ 
        // in case taht focus is on popover dialog do not turn off note menu
        if(!this.state.popoverOpen)
            this.setState({ hover: false });    
      }

      toggle() {
        this.setState({
          popoverOpen: !this.state.popoverOpen
        });
      }

    onRemove() {
        this.setState({
            isLoaded: false
        });
        $.ajax({
            url: constants.REST_URL + '/removeNote?noteId='+this.state.noteId,
            type: 'post',
            dataType: 'json',
            headers: {"Access-Control-Allow-Origin": "*"},
            contentType: 'application/json',
            success: function (data) {
                this.setState({
                    isLoaded: true
                });
            }.bind(this)
        });
        this.state.callback(this.state.noteId);
      }

    onArchivedClick(arch) {
        this.setState({
            isLoaded: false,
            archived: arch
        });
        constants.persistData(JSON.stringify({'dtype':this.state.dtype,
                                                'noteId': this.state.noteId,
                                                'archived': arch,
                                                'title':this.state.title,
                                                'content': this.state.text,
                                                'color': this.state.style,
                                                'imageSrc':this.state.src,
                                                'linkURL':this.state.linkurl}));
        this.state.callback(this.state.noteId);
    }

    onStyleChange(color) {
        this.setState({
            isLoaded: false,
            style:  color,
            popoverOpen: false
        });
        constants.persistData(JSON.stringify({'dtype':this.state.dtype,
                                        'color': color,
                                        'noteId': this.state.noteId,
                                        'archived': this.state.archived,
                                        'title':this.state.title,
                                        'content': this.state.text,
                                        'imageSrc':this.state.src,
                                        'linkURL':this.state.linkurl}));
    }

    render() {
        var st = "hidden";
        if(this.state.hover) st = "visible";
        return (
        <div className="noteFrame">
            <div onMouseOver={this.hoverOn.bind(this)} 
                 onMouseOut={this.hoverOff.bind(this)}
                 className={this.state.style + " note-wrapper"}>
                <div className="note-title">{this.state.title}</div>
                <div className="note-text">{this.state.text}</div>
                { !this.state.archived &&
                    <div id="note-footer" className="note-footer" style={{visibility: st}}>
                        <span id={'ColorIconTooltip-' + this.props.noteId} className="icon icon-color text-icon-footer" onClick={this.toggle.bind(this)} />
                        <UncontrolledTooltip placement="bottom" target={'ColorIconTooltip-' + this.props.noteId}>Change color</UncontrolledTooltip >
                        <Popover isOpen={this.state.popoverOpen} placement="top" target={'ColorIconTooltip-' + this.props.noteId}>
                        <PopoverBody><span onClick={this.onStyleChange.bind(this,'circle-white')} className="circle circle-white" /> 
                                     <span onClick={this.onStyleChange.bind(this,'circle-red')} className="circle circle-red" /> 
                                     <span onClick={this.onStyleChange.bind(this,'circle-blue')} className="circle circle-blue" /> 
                                     <span onClick={this.onStyleChange.bind(this,'circle-gray')} className="circle circle-gray" />  
                        </PopoverBody>
                        </Popover>
                        <span id={'EditIconTooltip-' + this.props.noteId} 
                              data-toggle="modal" 
                              data-target={"#addeditnotemodal"+ this.props.noteId} 
                              className="icon icon-edit text-icon-footer" />
                        <UncontrolledTooltip placement="bottom" 
                                            target={'EditIconTooltip-' + this.props.noteId}>
                                            Edit note
                        </UncontrolledTooltip >
                        <span id={'TrashIconTooltip-' + this.props.noteId} 
                              onClick={this.onArchivedClick.bind(this,true)} 
                              className="icon icon-trash text-icon-footer"/>
                        <UncontrolledTooltip placement="bottom" 
                                             target={'TrashIconTooltip-' + this.props.noteId}>
                                             Delete note
                        </UncontrolledTooltip >
                        
                    </div> 
                }
                { this.state.archived &&
                    <div id="note-footer" className="note-footer" style={{visibility: st}}>
                        <span onClick={this.onArchivedClick.bind(this,false)} className="sendback">Send back to notes</span>
                        |
                        <span onClick={this.onRemove.bind(this)} className="remove">Remove</span>
                        
                    </div> 
                }
            </div>
        </div>
        );
    }
}

TextNote.propTypes = {
    title: PropTypes.string,
    text: PropTypes.string,
    style: PropTypes.string,
    callback: PropTypes.func
}

TextNote.defaultProps = {
    title: '-',
    text: '(no text specified)',
    style: ''
  };
