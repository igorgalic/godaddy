import React from "react";
import {NavLink} from "react-router-dom";

export const Menu = (props) => {
    return (
        <nav className="nav flex-column">
            <div>
                <div className="navbar-header">
                    <ul className="nav flex-column">
                        <div id="sidebar-logo"> <img src="./Illustrations/note-tracker-logo.svg" />
                        </div>
                        <li className="nav-item">
                            <NavLink exact activeClassName={"active"} to={"/allnotes"} >
                                <span className="icon icon-all-notes"/> All notes
                            </NavLink>
                        </li>
                        <li className="nav-subitem"><NavLink activeClassName={"active"} to={"/textnotes"} >
                        <span className="icon icon-notes"/> Notes</NavLink></li>
                        <li className="nav-subitem"><NavLink activeClassName={"active"} to={"/imagenotes"} >
                        <span className="icon icon-images"/> Images</NavLink></li>
                        <li className="nav-subitem"><NavLink activeClassName={"active"} to={"/linknotes"} >
                        <span className="icon icon-link"/> 
                        Links</NavLink></li>
                        <hr className="sidebar-separator"/>
                        <li className="nav-item"><NavLink activeClassName={"active"} to={"/trash"} >
                        <span className="icon icon-trash"/> Trash</NavLink></li>
                    </ul>
                </div>
            </div>
        </nav>
    );
};