import React from "react";
import { Redirect } from "react-router-dom";

export class ConfirmDialog extends React.Component {
    
    render() {
        return (
            <div className="modal fade" id="confirmmodal" aria-hidden="true" role="dialog">
                <div className="modal-dialog extension modal-dialog-centered" role="document">       
                    <div className="modal-content">
                        <div className="modal-header">
                        <h4 className="modal-title bold float-left">Add/Edit Note</h4>
                        </div>
                        
                            <div className="confirm-body">
                            <div><span className="icon icon-notes"/></div>
                                <div>Note has been added</div>
                
                                <div className="confirm-footer">
                                    <button type="button" 
                                            className="btn btn-green btn-lg round" 
                                            data-dismiss="modal"
                                            data-toggle="modal">
                                            OK
                                    </button>
                                </div>
                            </div>
                        
                    </div> 
                </div>
            </div>
        );
    }
}
