import * as constants  from "../constants";
import React from "react";
import PropTypes from "prop-types";
import {UncontrolledTooltip } from "reactstrap";

export class LinkNote extends React.Component {
    constructor(props){
        super();
        this.state= {
            noteId: props.noteId,
            linkurl: props.linkurl,
            archived: props.archived,
            hover:false,
            callback: props.callback
        }
    }
    hoverOn(){
        this.setState({ hover: true });
      }
    hoverOff(){ 
        this.setState({ hover: false });    
      }

      onRemove() {
        this.setState({
            isLoaded: false
        });
        $.ajax({
            url: constants.REST_URL + '/removeNote?noteId='+this.state.noteId,
            type: 'post',
            dataType: 'json',
            headers: {"Access-Control-Allow-Origin": "*"},
            contentType: 'application/json',
            success: function (data) {
                this.setState({
                    isLoaded: true
                });
            }.bind(this)
        });
        this.state.callback(this.state.noteId);
      }

    onArchivedClick(arch) {
        this.setState({
            isLoaded: false,
            archived: arch
        });
        constants.persistData(JSON.stringify({'dtype':constants.NOTE_TYPE_LINK,
                                                'noteId': this.state.noteId,
                                                'linkURL':this.state.linkurl,
                                                'archived': arch}));
        this.state.callback(this.state.noteId);
    }

    render() {
        var st = "hidden";
        if(this.state.hover) st = "visible"
        return (
        <div className="noteFrame">
            <div onMouseOver={this.hoverOn.bind(this)} 
                 onMouseOut={this.hoverOff.bind(this)}
                 className="link-note-wrapper">
                 <a href={this.state.linkurl} target="_blank" className="float-right"><span id="externLink" className="icon icon-new-tab"/></a>
                <span className="text-primary">{this.state.linkurl}</span>
                { !this.state.archived &&
                    <div id={"footer"  + this.props.noteId} className="link-note-footer" style={{visibility: st}}>
                        <span id={'EditIconTooltip-' + this.props.noteId} 
                                data-toggle="modal" 
                                data-target={"#addeditnotemodal"+ this.props.noteId} 
                                className="icon icon-edit text-icon-footer" />
                        <UncontrolledTooltip placement="bottom" 
                                            target={'EditIconTooltip-' + this.props.noteId}>
                                            Edit note
                        </UncontrolledTooltip >
                        <span id={'TrashIconTooltip-' + this.props.noteId} 
                                onClick={this.onArchivedClick.bind(this,true)} 
                                className="icon icon-trash text-icon-footer"/>
                        <UncontrolledTooltip placement="bottom" 
                                                target={'TrashIconTooltip-' + this.props.noteId}>
                                                Delete note
                        </UncontrolledTooltip >
                    </div>
                }
                { this.state.archived &&
                    <div id="footer" className="link-note-footer" style={{visibility: st}}>
                        
                        <span onClick={this.onArchivedClick.bind(this,false)} className="sendback">Send back to notes</span>
                        |
                        <span onClick={this.onRemove.bind(this)} className="remove">Remove</span>
                        
                    </div> 
                }
            </div>
        </div>
        );
    }
}

LinkNote.propTypes = {
    linkurl: PropTypes.string,
    callback: PropTypes.func
}

LinkNote.defaultProps = {
    linkurl: '(no URL specified)'
  };