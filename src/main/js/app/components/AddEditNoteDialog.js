import * as constants from "../constants";
import React from "react";
import PropTypes from "prop-types";
import { ConfirmDialog } from "./ConfirmDialog";
import { isNullOrUndefined } from "util";
import {Redirect} from "react-router-dom";

export class AddEditNoteDialog extends React.Component {

    constructor(props) {
        super(props);
        this._isMounted=false;
        this.state = { 
            noteType: props.noteType,
            noteId: props.noteId,
            title:props.title,
            text:props.text,
            linkurl:props.linkurl,
            src: props.src,
            callback: props.callback,
            color: props.color,
            result: false
        };
      }
    
      handleNoteChange(note) {
        this.setState({
            noteType: note
        });
      }

      handleImageChange() {
        this.setState({
            noteType: constants.NOTE_TYPE_IMAGE
        });
      }

      handleLinkChange() {
        this.setState({
            noteType: constants.NOTE_TYPE_LINK
        });
      }

      titleChanged(event) {
        this.setState({
            title: event.target.value
        });
      }

      textChanged(event) {
        this.setState({
            text: event.target.value
        });
      }

      srcChanged(event) {
        this.setState({
            src: event.target.value
        });
      }

      linkurlChanged(event) {
        this.setState({
            linkurl: event.target.value
        });
      }

      onAddNote(){
        this.setState({
            result: false
        });
        $.ajax({
            url: constants.REST_URL+'/saveNote',
            type: 'post',
            dataType: 'json',
            data: JSON.stringify({'dtype':this.state.noteType,
                                  'noteId': this.state.noteId,
                                  'title':this.state.title,
                                  'content': this.state.text,
                                  'imageSrc':this.state.src,
                                  'color':this.state.color,
                                  'linkURL':this.state.linkurl}),
            headers: {"Access-Control-Allow-Origin": "*"},
            contentType: 'application/json',
            success: function (data) {
                if(this._isMounted)
                    this.setState({
                        result: true
                    });
                if (!isNullOrUndefined(this.state.callback))
                    this.state.callback(data);
            }.bind(this)
        });
      }

      componentDidMount(){
        this._isMounted=true;
    }

    componentWillUnmount(){
        this._isMounted=false;
    }

    render() {
        if (this.state.result && this.state.noteId<=0) {
            this.state.result=false; // do not  need to re render          
            return <Redirect to="/allnotes"/>;
        }
        return (
        <div className="modal fade" id={"addeditnotemodal"+ this.state.noteId} role="dialog">
            <div className="modal-dialog extension modal-dialog-centered" role="document" >     
                <div className="modal-content">
                    <div className="modal-header">
                    <h4 className="modal-title bold float-left">Add/Edit Note</h4>
                    </div>
                    <form>
                        <div className="modal-body extension">
                        <div className="modal-padding">Choose what kind of note you would like to add:</div>
                        <span className="form-check form-check-inline modal-padding">
                            <input id="notetextradio" 
                                   className="form-check-input" 
                                   type="radio" 
                                   name="noteType" 
                                   value={constants.NOTE_TYPE_TEXT} 
                                   checked={this.state.noteType===constants.NOTE_TYPE_TEXT} 
                                   onChange={ this.handleNoteChange.bind(this,constants.NOTE_TYPE_TEXT) }/>
                            <label className="form-check-label" htmlFor="notetextradio">
                                <span className="icon icon-notes"/> 
                                Note
                                
                            </label>
                        </span>
                        <span className="form-check form-check-inline modal-padding">
                            <input id="imageradio" 
                                   className="form-check-input" 
                                   type="radio" 
                                   name="noteType" 
                                   value={constants.NOTE_TYPE_IMAGE} 
                                   checked={ this.state.noteType===constants.NOTE_TYPE_IMAGE } 
                                   onChange={ this.handleImageChange.bind(this) }/>
                            <label className="form-check-label" htmlFor="imageradio">
                                <span className="icon icon-images"/> 
                                Image
                            </label>
                        </span>
                        <span className="form-check form-check-inline modal-padding">
                            <input id="linkradio" 
                                   className="form-check-input" 
                                   type="radio" 
                                   name="noteType" 
                                   value={constants.NOTE_TYPE_LINK}
                                   checked={ this.state.noteType===constants.NOTE_TYPE_LINK } 
                                   onChange={ this.handleLinkChange.bind(this) }/>
                            <label className="form-check-label" htmlFor="linkradio">
                                <span className="icon icon-link"/> 
                                Link
                            </label>
                        </span>

                        {this.state.noteType===constants.NOTE_TYPE_TEXT &&
                        <div id="inputFields">
                            <input className="form-input modal-inputfield" 
                                   id="noteTitle" 
                                   type="text" 
                                   placeholder="Title" 
                                   value={this.state.title}
                                   onChange={this.titleChanged.bind(this)}/>
                            <input className="form-input modal-inputfield" 
                                   id="noteText" 
                                   type="text" 
                                   placeholder="Add note text..." 
                                   value={this.state.text}
                                   onChange={this.textChanged.bind(this)}/>
                        </div>
                        }

                        {this.state.noteType===constants.NOTE_TYPE_IMAGE &&
                        <div id="inputFields">
                            <input className="form-input modal-inputfield" 
                                   id="imageURL" 
                                   type="text" 
                                   placeholder="Image URL"
                                   value={this.state.src}
                                   onChange={this.srcChanged.bind(this)}/>
                        </div>
                        }

                        {this.state.noteType===constants.NOTE_TYPE_LINK &&
                        <div id="inputFields">
                            <input className="form-input modal-inputfield" 
                                   id="linkURL" 
                                   type="text" 
                                   placeholder="Link URL"
                                   value={this.state.linkurl}
                                   onChange={this.linkurlChanged.bind(this)}/>
                        </div>
                        }
                        </div>
                        <div className="modal-footer modal-buttons">
                            <button onClick={this.onAddNote.bind(this)} 
                                    type="button" 
                                    className="btn btn-green btn-lg round"
                                    data-toggle="modal"
                                    data-dismiss="modal"
                                    data-target="#confirmmodal">
                                    Add A Note
                            </button>
                            <button type="button" 
                                    className="btn btn-close btn-lg round" 
                                    data-dismiss="modal">
                                    Close
                            </button>
                        </div>
                    </form>
                </div> 
            </div>
        </div>
        );
    }
}

AddEditNoteDialog.propTypes = {
    nodeId: PropTypes.number,
    title: PropTypes.string,
    text: PropTypes.string,
    linkurl: PropTypes.string,
    src: PropTypes.string,
    noteType: PropTypes.string
}

AddEditNoteDialog.defaultProps = {
    noteType: constants.NOTE_TYPE_TEXT,
    title: '',
    text: '',
    linkurl: '',
    src: '',
    noteId: 0
  };