import * as constants  from "../constants";
import React from "react";
import PropTypes from "prop-types";
import { AddEditNoteDialog } from "./AddEditNoteDialog";
import {UncontrolledTooltip } from "reactstrap";

export class ImageNote extends React.Component {
    constructor(props){
        super(props);

        this.state= {
            noteId: props.noteId,
            src: props.src,
            archived: props.archived,
            hover: false,
            callback: props.callback
        }
    }

    hoverOn(){
        this.setState({ hover: true });
      }
    hoverOff(){ 
        this.setState({ hover: false });    
      }


      onRemove() {
        this.setState({
            isLoaded: false
        });
        $.ajax({
            url: constants.REST_URL + '/removeNote?noteId='+this.state.noteId,
            type: 'post',
            dataType: 'json',
            headers: {"Access-Control-Allow-Origin": "*"},
            contentType: 'application/json',
            success: function (data) {
                this.setState({
                    isLoaded: true
                });
            }.bind(this)
        });
        this.state.callback(this.state.noteId);
      }

      onEditClick() {
        <AddEditNoteDialog />
    }

    onArchivedClick(arch) {
        this.setState({
            isLoaded: false,
            archived: arch
        });
        constants.persistData(JSON.stringify({'dtype':constants.NOTE_TYPE_IMAGE,
                                        'noteId': this.state.noteId,
                                        'archived': arch,
                                        'imageSrc':this.state.src}));
        this.state.callback(this.state.noteId);
    }

    render() {
        var st = "hidden";
        if(this.state.hover) st = "visible"
        return (
        <div className="noteFrame">
            <div onMouseOver={this.hoverOn.bind(this)} 
                 onMouseOut={this.hoverOff.bind(this)}
                 className="image-note-wrapper">
                <img src={this.state.src} alt="Image URL is not valid!"/>
                
                { !this.state.archived &&
                    <div id="footer" className="image-note-footer" style={{visibility: st}}>
                        <span id={'EditIconTooltip-' + this.props.noteId} 
                                data-toggle="modal" 
                                data-target={"#addeditnotemodal"+ this.props.noteId} 
                                className="icon icon-edit text-icon-footer" />
                        <UncontrolledTooltip placement="bottom" 
                                            target={'EditIconTooltip-' + this.props.noteId}>
                                            Edit note
                        </UncontrolledTooltip >
                        <span id={'TrashIconTooltip-' + this.props.noteId} 
                                onClick={this.onArchivedClick.bind(this,true)} 
                                className="icon icon-trash text-icon-footer"/>
                        <UncontrolledTooltip placement="bottom" 
                                                target={'TrashIconTooltip-' + this.props.noteId}>
                                                Delete note
                        </UncontrolledTooltip >
                    </div>
                }
                { this.state.archived &&
                    <div id="footer" className="image-note-footer" style={{visibility: st}}>
                        
                        <span onClick={this.onArchivedClick.bind(this,false)} className="sendback">Send back to notes</span>
                        |
                        <span onClick={this.onRemove.bind(this)} className="remove">Remove</span>
                        
                    </div> 
                }
            </div>
        </div>
        );
    }
}

ImageNote.propTypes = {
    src: PropTypes.string,
    callback: PropTypes.func
}

ImageNote.defaultProps = {
    src: '-'
  };