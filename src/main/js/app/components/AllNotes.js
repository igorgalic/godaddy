import * as constants from "../constants" ;
import React from "react";
import {TextNote} from "./TextNote";
import {ImageNote} from "./ImageNote";
import {LinkNote} from "./LinkNote";
import { Header } from "./Header";
import { isUndefined } from "util";
import {AddEditNoteDialog} from "./AddEditNoteDialog";
import { EmptyTrash } from "./EmptyTrash";
import { NoNotes } from "./NoNotes";
import { ConfirmDialog } from "./ConfirmDialog";
export class AllNotes extends React.Component {

    /** 
     * @param props This react component can be constructed with restURL props or
     * it will be constructed with default restURL property
      */
     constructor(props){
        super(props);
        /** RestURL is not changing in this component liftime so it does not need to be a state
         * @default restURL is set as (constants.REST_URL +"/getall?type="+constants.ALL_TYPES)
         */
        this.restURL=isUndefined(props.restURL)
                        ?(constants.REST_URL +"/getall?type="+constants.ALL_TYPES)
                        :props.restURL;

        // Archived flag is not changing in this component liftime so it does not need to be a state
        this.archived=false;

        //fix for asynch memory leaks
        this._isMounted=false;
    }

    componentDidMount()
    {
        $.ajax({
            url: this.restURL,
            type: 'post',
            dataType: 'json',
            headers: {"Access-Control-Allow-Origin": "*"},
            contentType: 'application/json',
            success: function (data) {
                if (this._isMounted)
                    this.setState({
                        notes: data
                    });
            }.bind(this)
        });
        this._isMounted=true;
    };

    componentWillUnmount(){
        this._isMounted=false;
    }

    callbackClearList()
    {
        this.setState({
            notes: []
        })
    }

    callbackReplaceComponent(in_note){
        var noteList = this.state.notes;
        for (var i = 0; i < this.state.notes.length; i++) {
            var note = noteList[i];
            //searching for note with specified id
            if (note.noteId===in_note.noteId) {
                //replacing the note with new values
                noteList[i]=in_note;
                noteList.splice(i, 1);
                //forcing a state change
                //only updating elelemnt in array doeas not trigger re-render
                //reducing state array list do so instead of simple replace
                //we doo remove then insert
                this.setState({
                    notes: noteList
                });
                //inserting element on same index
                noteList.splice(i, 0,in_note);
                this.setState({
                    notes: noteList
                });
                return;
            }
        }
        // if it is not edited then it is added to the list
        noteList.splice(0, 0,in_note);
        this.setState({
            notes: noteList
        });
        return;
    }

    callbackRemoveComponent(id)
    {
        var noteList = this.state.notes;
        for (var i = 0; i < noteList.length; i++) {
            var note = noteList[i];
            //searching for note with specified id
            if (note.noteId===id) {
                //removing the note with specifiedid from the list
                noteList.splice(i, 1);
                //forcing a state change
                this.setState({
                    notes: noteList
                });
                // exit there is only one note for removal
                return;
            }
        }
    }

    render() {
        /* if for some reason ajax call in componentWillMount do not finish on time
        * display loading message
        */
        if (this.state==null || isUndefined(this.state.notes)) 
            return <div className="progress">
                        <p>loading...</p>
                    </div>;
        // if it is not ready or there is no notes use emtp pages for lists
        if( this.state.notes.length===0) 
            {
                if (this.archived) return <EmptyTrash />;
                else return  <NoNotes callback={this.callbackReplaceComponent.bind(this)}/>;
            }

        var notesElements=[];
        var modals=[];
        // for each JSON object make corresponding React component
        this.state.notes.forEach(note => {
           
            modals.push(<AddEditNoteDialog key={note.noteId}
                                            noteType={note.dtype}
                                            noteId={note.noteId}
                                            title={note.title}    
                                            text={note.content}
                                            src={note.imageSrc}
                                            color={note.color}
                                            linkurl={note.linkURL}
                                            callback={this.callbackReplaceComponent.bind(this)}
                                            />);
            if(note.dtype===constants.NOTE_TYPE_TEXT) 
                notesElements.push(<TextNote key={note.noteId}                                              
                                             noteId={note.noteId}
                                             dtype={note.dtype} 
                                             title={note.title} 
                                             text={note.content} 
                                             className={note.color}
                                             archived={this.archived} 
                                             callback={this.callbackRemoveComponent.bind(this)}
                                             />);
            if(note.dtype===constants.NOTE_TYPE_IMAGE) 
                notesElements.push(<ImageNote key={note.noteId} 
                                              noteId={note.noteId}
                                              dtype={note.dtype} 
                                              src={note.imageSrc}
                                              archived={this.archived}
                                              callback={this.callbackRemoveComponent.bind(this)}/>);
            if(note.dtype===constants.NOTE_TYPE_LINK) 
                notesElements.push(<LinkNote key={note.noteId} 
                                             noteId={note.noteId}
                                             dtype={note.dtype} 
                                             linkurl={note.linkURL}
                                             archived={this.archived}
                                             callback={this.callbackRemoveComponent.bind(this)}/>);                                                                                                                                            
        });
        return (
            <div>
                <Header isArchive={this.archived} callback={this.callbackClearList.bind(this)} callbackEdit={this.callbackReplaceComponent.bind(this)}/>
                <div className="mainGrid">
                    {notesElements}
                </div>
                {modals}
            </div>
        );
    };
}