import * as constants from "../constants" 
import { AllNotes } from "./AllNotes";
import { isUndefined } from "util";
/**
 * @class ImageNotesList is extension of @class AllNotes
 * It has all the methods from the AllNotes but it constructs with different parameters
 */
export class ImageNotesList extends AllNotes {

    /** 
     * ImageNotesList is a extension of the AllNotes component with archived argument set to false
     * @param props This react component can be constructed with restURL props or
     * it will be constructed with default restURL property
      */
    constructor(props){
        super(props);

        /** RestURL is not changing in this component liftime so it does not need to be a state
         * @default restURL is set as (constants.REST_URL +"/getall?type="+constants.NOTE_TYPE_IMAGE)
         */
        this.restURL=isUndefined(props.restURL)
                        ?(constants.REST_URL +"/getall?type="+constants.NOTE_TYPE_IMAGE)
                        :props.restURL;

        // Archived flag is not changing in this component liftime so it does not need to be a state
        this.archived=false;
    }
}