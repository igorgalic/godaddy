import React from "react";
import {AddEditNoteDialog} from "./AddEditNoteDialog";

export class NoNotes extends React.Component {
    constructor(props){
        super(props);
    }
    
    render() {
        return (
            <div id="emptyTrash">
                <div id="emptyTrashImg">       
                <img src="./Illustrations/notes-empty-state.svg" alt="Home page Image"/>
                </div>
                <div id="emptyTrashText">       
                   Keep Track of Your Notes
                </div>
                <div id="emptyTrashSubText">       
                   Click on the button below to add your first note
                </div>
                <div id="emptyTrashButton">       
                   <button className="btn btn-defaut btn-green btn-lg round" data-toggle="modal" data-target="#addeditnotemodal-1">Add First Note</button>
                   <AddEditNoteDialog noteId="-1" key="addFirstNote" callback={this.props.callback}/>
                </div>
            </div>
        );
    }
}