export const NOTE_TYPE_TEXT = 'TextNote';
export const NOTE_TYPE_IMAGE = 'ImageNote';
export const NOTE_TYPE_LINK = 'LinkNote';
export const ARCHIVE = 'archive';
export const ALL_TYPES = 'all';

//export const REST_URL = 'http://localhost:8080/notes';
export const REST_URL = '/notes';

export function persistData(jsonData) {
    $.ajax({
        url: REST_URL+'/saveNote',
        type: 'post',
        dataType: 'json',
        data: jsonData,
        headers: {"Access-Control-Allow-Origin": "*"},
        contentType: 'application/json',
        success: function (data) {
            return data;
            },
        error: function (data) {
            alert("There is a problem with saving changes in DB. Contact administrator!");
            },
        });
};