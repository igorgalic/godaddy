

module.exports = env => {
	var path = require("path");
	const no_dist = (env && env.dist === "false");
	var DIST_DIR = path.resolve(__dirname, "src/main/resources/static");
	var SRC_DIR = path.resolve(__dirname, "src/main/js");

    return {
    	    mode: 'production',
    	    entry: SRC_DIR + "/app/index.js",
    	    output: {
    	        path: DIST_DIR + "/app",
    	        filename: "bundle.js",
    	        publicPath: "/app/"
    	    },
    	    module: {
    	        rules: [
    	            {
    	                test: /\.js?/,
    	                include: SRC_DIR,
    	                loader: "babel-loader",
    	                query: {
    	                    presets: ["react", "es2015", "stage-2"]
    	                }
    	            }
    	        ]
    	    }
    	};
      }